Title: Monthly Report January 2019
Date: 2019-02-08
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped down of the February 2019 newletter sent out to members of Codeberg e.V one week ago)

Dear Codeberg e.V. Members and Supporters!

We are now looking back to our first month after launching codeberg.org, and it is time for some updates.

As of today, Codeberg e.V. has 25 active members. We are hosting 333 repositories, created and maintained by 379 users.

Our machines are runnnig at less than 25% capacity, we will scale up as soon we come close to the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

As usual, the new year brought us bad news and good news. Launching the platform caused far less technical difficulties than expected, and we experienced a lot of positive feedback. Thanks to all for this!

But obviously not everything went smooth. All user repos were migrated without loss. But migrating our own teahub->codeberg Infrastructure issue tracker, we missed some issues. The problem was caused by the existence of multiple diverging copies on the various testing servers, which require manual merging that we avoided due to higher-priority issues during launch. The issues are probably not essential but could surely be useful and we are confident to recover them completely as soon time permits.

What are our next steps? Obviously we want to enhance the platform, every helping hand is welcome! First users have collected and contributed quite a number of interesting ideas in issue https://codeberg.org/Codeberg/Community/issues/23, if we even realize just a few of them, that would make a great improvement!

Concerning the organisation of the Codeberg e.V. we would like to establish a non-public group on codeberg.org, where we can host regular update reports for members, discuss decisions in advance, and hold polls. For this we need a few more weeks, as we would like to protect the privacy of all our members (avoid even leaking membership information unless you chose to show yourself).

The most important priority right now is attracting a sustainable user base and strengthening the social network of developers meeting on Codeberg.org.

Let's spread the word! We are looking forward to exciting times.

