Title: One year into the war, your help is still needed
Date: 2023-11-10
Category: Letters from Codeberg
Authors: Codeberg e. V.

*(This text has been written a rather long time ago, with discussions starting in
Spring 2023. It took a while to draft, have Codeberg e. V. members vote and to finally
publish it. The latter was also due to other more important things in our
work queue. We apologize for the delay.)*

Since the beginning of the Russian invasion more than a year ago, a horrific war has been raging in Ukraine,
while Crimea and Eastern Ukraine have been under Russian occupation for much longer. We find this unaccept-
able. Countless people have already been forced to flee their homes and to seek refuge in other countries. Cities
have been completely destroyed by Russian bombardment, many more are severely damaged; Kherson, Odesa
and Bakhmut are only a few of many examples. Ukrainian children are systematically abducted, and far too many
mass graves of innocent civilians have already been found in liberated areas.

These appalling events have not gone unnoticed. And like us, you may wonder what we can do about it. We may
not be not doctors, firefighters or a human rights organisation — our primary responsibility is promoting the
values of software freedom —, but we are convinced that war is an issue that concerns us and society as a whole.

Software freedom is not just about your preferred software licence or your favourite text editor. It is also about
the values of community, free culture and ideals. In all the places where human dignity and the right to free
expression are not respected and safeguarded, these values will eventually cease to exist.

War does not just take away developers to work on our beloved open-source software. Apart from the human
toll and the devastation being horrific beyond human imagination on its own, it manifests authoritarian and
impoverishing environments that foster despair over creativity. A notable example of that from the open-source
community is the story of [Bassel Khartabil](https://www.wired.com/story/free-bassel-essay),
a free culture contributor who was secretly executed by the regime in Syria.

Therefore, in line with the goals of our association, we believe that we should direct you to the following projects
and organisations that rely on contributions by volunteers:

- [Snowflake](https://snowflake.torproject.org/) a web extension that you can download to help people access the free Internet. As it is mainly used
in places where [publicized Tor relays](https://metrics.torproject.org/) are blocked, your IP address will not be made public. Also, it does not
require any technical knowledge to use it.
- [OONI](https://ooni.org/)

If you do not have the time but have some money to spare, we collected a few organisations that work on im-
proving the conditions of the civilians present in Ukraine and Russia, as well as across the globe:

- [Doctors without Borders](https://www.msf.org/ukraine), which provides medical emergency assistance during crisis and war;
- [War Child](https://www.warchild.org/), an organisation whose effort is to help children in conflict areas;
- [Aktionsbündnis Katastrophenhilfe](https://www.aktionsbuendnis-katastrophenhilfe.de/krieg-in-der-ukraine), which is a collaboration of the Red Cross, UNICEF and Caritas (German
language only).

Finally, we think it is important to stay informed and verify information before passing it on, as this war is also
fought with propaganda and misinformation.

Here are some news sources we would like to recommend to you:

- [The Ukraine News of The Guardian](https://www.theguardian.com/world/ukraine), which is independent and financed by its readers;
- [The Intercept](https://theintercept.com/), which was founded by the leading journalists in the Snowden revelations;
- [Bellingcat](https://bellingcat.com/), an investigative research network of Open Source Intelligence journalists.

Let us respect each other instead of redrawing borders through military violence. We sincerely hope that this
conflict gets resolved soon – without further bloodshed and civilian harm.

Best regards,  
Your Codeberg e.V.