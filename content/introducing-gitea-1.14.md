Title: Introducing Gitea 1.14
Date: 2021-04-29
Category: Announcement
Authors: Codeberg e.V.

We are happy to announce that we are finally - with some delay -  deploying Gitea 1.14 with a bunch of new features, bugfixes and other improvements.
We are thankful to all the contributors of Gitea for their awesome work. We enjoy using their software to build a true community-based software forge and we are already looking forward to see the next release with a bunch of further improvements.

This blog post will introduce you to the most important new features you'll love. If you want to know more, feel free to check out the full changelog at the [GitHub release page](https://github.com/go-gitea/gitea/releases/tag/v1.14.0).

**Issues from file view:** Ever looked at some file to notice something odd? You can now immediately create an issue when viewing a file. Just click on the line number to see a popup offering you to reference this line in a new issue.

**Dismiss a review:** A review is not relevant but is either confusing your contributors (because it looks like a blocker) or is even technically blocking a merge for your protected branch? You can now simply dismiss a review. Pro tip: We recommend looking at it first 🙃.

**Mark pull request as manually merged:** There are situations where your manually pushed merge is not automatically detected. You'll now find a button that helps mitigate this situation. You can even enter a hash to link the manual commit.

**Image diffs:** Do you like spot-the-difference puzzles? Well, but they stop being funny when you just can't spot the difference someone committed to your assets. Gitea now comes with an improved diff view with three comparing modes to help you spot the differences. *[See an animation on GitHub](https://github.com/go-gitea/gitea/pull/14450)*

**SVGs are finally displayed:** Only a tiny change, but if you're working with assets you'll like it. Your SVGs are now rendered instead of dumping the source code in the default view.

**Filter dashboard by team:** Your organization dashboard is too noisy? You were three days off and can't keep track? You can now filter activities in the dashboard by team to help you get an overview of what's going on.

There were some additional changes we already backported to our Gitea fork. Also, there were many tiny improvements to the UI, the performance and the overall experience. We hope you enjoy these changes and using Codeberg.

## What else is in the pipe?

After the Gitea update, we are continuing our work on improvements in other domains of the Codeberg services. We are already looking forward to move to our own hardware later this year. A machine has already been ordered and we hope to put it live in the coming weeks, starting with infrastructure testing and a CI / CD service before the whole platform is migrated. Do you rely on CI services? We'd like to hear your opinion. Just drop us a comment in our [Feedback issue](https://codeberg.org/Codeberg/Community/issues/428).

## Contributing to Codeberg

Do you want to become part of our journey and help build and maintain a true community software forge? We are happy about every helping hand.
If you want to support Codeberg, feel free to check out the open community issues once the update is done (especially those that are [looking for contribution](https://codeberg.org/Codeberg/Community/issues?state=open&labels=105) and pick one up. Also, we encourage everyone to pick up a good first issue upstream at Gitea to improve the base Codeberg relies on. Again, we want to thank everyone who had their part in this Gitea release.

Also, feel free to drop us a donation that helps maintain the infrastructure and provide new features. And as always: Tell your friends!
