Title: Monthly Report May 2019
Date: 2019-06-22
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month)


Dear Codeberg e.V. Members and Supporters!

It is overdue time for some updates.

Codeberg e.V. has 33 active members. We are hosting 654 repositories, created and maintained by 593 users. Compared to one month ago, this is an organic growth of rate of +101 repositories (+18% month-over-month), and +95 users (+19%).

Our monthly expenses for domains and servers accumulated in May to and 16.21 EUR. The machines are runnnig at about 20% capacity, we will scale up as soon we approach the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

The details of our bylaw change proposal have been worked out and are visible via https://codeberg.org/hw/org/commit/e52e56cf91348a47294ac6d02d3b187e8bf2ab33. We are now waiting for feedback from authorities whether this wording is sufficient to fulfil the change request required for recognition as Non-Profit organization by tax authorities. As soon we have this confirmation, we will propose this change as pull request, and will ask our members (you!) for vote via email.

Long-term, it would be great to have informative English translations of all relevant documents (some official documents are still German-only as this is the language required by local authorities). If you would like to contribute, translations, PRs in the org repository are very welcome!

Codeberg e.V.

