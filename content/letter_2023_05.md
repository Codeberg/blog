Title: Letter from Codeberg: May 2023
Date: 2023-05-15
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the news letters sent out to members of Codeberg e.V. – not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*


Dear Codeberg e.V. Members and supporters!

Our Annual Assembly for Codeberg e.V. legal members is nearing (May 21).
We now share some news from the past months at Codeberg.


Highlights
==========

**Forgejo 1.19**

With the availability of Forgejo 1.19 and after undergoing heavy testing,
we deployed Forgejo 1.19 by the end of April.
Forgejo is a software forge created on top of Gitea by an independent community
under the umbrella of Codeberg.

When this project was started last year,
we looked into an uncertain future.
While the project started as a soft-fork,
many feared that upstream collaboration would cease,
leading to a hard fork from Gitea.

Now, after running Forgejo for several months,
we are glad to have dared this step.
The Forgejo community offers something of incredible value: awesome support.
Since the Forgejo developers are like-minded
and with their use of Codeberg much closer to us,
collaboration on important issues drastically improved.

But the good news goes on:
The collaboration between Forgejo and Gitea also improved,
compared to how Codeberg used to work with Gitea in the past.
When some people wonder about the difference between Forgejo and Gitea (next to branding),
there is not much to show.
This is because significant work is being upstreamed to benefit Gitea users alike.
Although our direct contact with the Gitea developers was reduced,
we are happy that this dependency chain now allows for a more efficient workflow.


**Office Moved**

Codeberg does not have a "headquarter", our office was merely a room with a desk
where we could occassionally meet for office work.
This was sponsored by an individual,
but the cost was immense.
We agree that the money is probably better saved and invested elsewhere,
and decided to switch to a cheaper option.
We now found a new space in Berlin and moved over.


**Enjoy our achievements**

We were scratching our heads to look for more important news to share.
While scrolling through our chats, and social media profiles, and emails,
we saw an immense amount of positive feedback which we want to share.

More than 300 members have joined the Codeberg e.V. so far and are supporting our mission.
We are providing nearly 60,000 users and their 70,000 projects with Git hosting,
hosted Woodpecker CI, Weblate translation services and Codeberg Pages.

Codeberg is no small niche space,
but an emerging community of Free Software contributors.
Software hosted on Codeberg is available on F-Droid, as Debian and flatpak packages,
and libraries for many programming languages and frameworks can be obtained from Codeberg.

When browsing the web or Mastodon network,
we indirectly discover more and more projects which are hosted on Codeberg by coincidence.
Many are proud to have made the switch.

Although there is a lot of work to do and many problems to solve,
hearing positive voices of our users is an encouraging driver for our mission.
Together, we keep going!


Current Focus
=============

Our time is currently invested into the preparations of the Annual Assembly,
including formal requests, preparing the votes and economic plans.

Also, while Codeberg was running stable from a technical perspective,
we have seen resource abuse increase and noticed a several short downtimes
connected to excessive resource usage of single projects / users.
Our reaction time was within time-frames you can expect from a volunteer-driven project,
still we want to become better and improve our reaction to resource abuse
before it impacts the service quality.
Codeberg is run by our hardware and our nerves. Please don't overload either of those.



Where we appreciate help
========================

We published an honest blog article about ["The hardest scaling issue"](https://blog.codeberg.org/the-hardest-scaling-issue.html).

If you want to help solving it,
we appreciate your helping hand.
Most urgently needed are people who can take responsibility of a project independently
and coordinate volunteer contributions therein.

For example, you could help set up and maintain Code Search,
see [this issue](https://codeberg.org/Codeberg/Community/issues/904).
Or, you could join maintenance (code or sysadmin) of
Codeberg Pages, Weblate, Woodpecker CI,
or help us maintain LXC containers, HAProxy, Ceph filesystem and more.
Please let us know!

Our community issue tracker at [Codeberg/Community](https://codeberg.org/Codeberg/Community/issues) remains an active place to discuss bugs, features, and community-related needs. If you have some free cycles, please go through the recent issues, help out users, forward bug reports and feature requests to upstream Forgejo at [Codeberg](https://codeberg.org/forgejo/forgejo/issues) if they aren't reported yet, and consider joining our group of active community maintainers: see [Codeberg/Community#571](https://codeberg.org/Codeberg/Community/issues/571) for details.


Other
=====

Codeberg e.V. has 331 members in total, these are 236 members with active voting rights, 93 supporting members and 2 honorary members.

*Friendly reminder: membership in the account group "Members" on Codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*


--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Arminiusstraße 2 - 4 – 10551 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.

