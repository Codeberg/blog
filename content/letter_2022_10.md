Title: Letter from Codeberg: Hackathon, Translation Service & more
Date: 2022-10-25
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the news letters sent out to members of Codeberg e.V. – not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

It's time for some updates,
we think there are some great news to share with you.
If you like what you read here, please consider a [donation to Codeberg](https://docs.codeberg.org/improving-codeberg/#donate-to-codeberg)
to support our mission.
Thank you very much for your trust and support!

Highlights
==========

### Infrastructure stability

Although our infrastructure is not yet back at the stability level of the past years,
we managed to reduce our issues by enforcing tighter rate-limiting.
We have chosen a flexible approach which blocks single IP addresses
which account for more than 4% of all requests to our main service,
but only if system load exceeds a certain threshold.

This will hopefully ensure that legit users (e.g. heavy CI jobs or rate limits) don't get throttled
when our systems are idle and could handle the load well.
Please let us know if these measures affect your work. We appreciate your feedback.

These changes still need improvements and cleanup (especially graceful communication to the end user),
but we are happy that this allowed us to automatically react to crawlers and botnets
(which often required manual action in the past).

Our current focus is on moving services quickly but careful to our own and more powerful hardware.


### Hackathon November 4th to 6th

We're planning a next hackathon on November 4th to 6th.
Please mark this date in your calendars,
it's a great opportunity to support Codeberg and get to know other members and users.
You can read all about the event in the [dedicated blog article](https://blog.codeberg.org/codeberg-hackathon-goes-v2.html).
We're looking forward to seeing you.


### Translation Service

We are happy to announce that we got a Weblate instance running at
[translate.codeberg.org](https://translate.codeberg.org).
This allows projects to manage translations just a few clicks away from their projects,
and every Codeberg user to easily provide some translations in the languages they speak.
This service is very new, and we're considering it "public beta",
but we're confident that Weblate is running fine.
Thanks to the Weblate Contributors for making this possible with their Free Software.


Heads Up
========

### Mastodon account moved

Our Mastodon account moved over to [@Codeberg@social.anoxinon.de](https://social.anoxinon.de/@codeberg),
because our previous host is closing down.
The move went very smooth, and we are happy about our new home at long-term Codeberg users and supporters: Anoxinon e.V.
If your account has not yet been moved over, please manually ensure that you follow our new account there.


### Status Page

If something on Codeberg does not work the proper way,
you can now find a status dashboard at [status.codeberg.org](https://status.codeberg.org)
or [status.codeberg.eu](https://status.codeberg.eu).
Feel free to add it to your bookmarks – in case of emergency.
We hope that you don't need to look at it very often.


### Open Data

We're now publishing regular data about our systems at [Codeberg-Infrastructure/opendata](https://codeberg.org/Codeberg-Infrastructure/opendata)
and planning to extend this.
Please leave a comment about what kind of data you're interested in,
or submit a script for further analysis.


Where we appreciate help
========================

As mentioned above, we're happy to see your contribution during the Hackathon.
It's not only an opportunity to meet the community,
but also to get involved with the machinery behind codeberg.org, and learn about Gitea, Go and all the tools at Codeberg.

If you have some spare time during the next weeks,
we are looking for help with moving our main mail server and setting it up on the bare metal machine
including spam filtering.
Please reach out to otto@codeberg.org if this job is yours :)

Our moderation and community issue tracker at [Codeberg/Community](https://codeberg.org/Codeberg/Community/issues) remains an active place to discuss bugs, features, and community-related needs. If you have some free cycles, please go through the recent issues, help out users, forward bug reports and feature requests to upstream Gitea at [GitHub](https://github.com/go-gitea/gitea/issues) if they aren't reported yet, and consider joining our group of active community maintainers: see [Codeberg/Community#571](https://codeberg.org/Codeberg/Community/issues/571) for details.



Number Games
============

We are hosting 42010 repositories, created and maintained by 34042 users. Compared to one month ago, this is an organic growth rate of +3372 repositories (+8.7% month-over-month) and +2271 users (+7.1%).

This data can also be found as a daily history at [Codeberg-Infrastructure/opendata](https://codeberg.org/Codeberg-Infrastructure/opendata/)

Our SSD-backed VPS is running at about 92% storage capacity. Since we are easily able to move data to our Ceph cluster (usage at 7%) and every bit on the SSD makes our service faster, we won't try to bring this down for now, but rather scale up the Ceph cluster when necessary. Once migration of the instance is due, the SSD data will be moved to the Ceph cluster.

Codeberg e.V. has 246 members in total, these are 175 members with active voting rights and 69 supporting members, and 2 honorary members.

Your Codeberg e.V.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*


--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.

