Title: More power for you – what a storage quota will bring
Date: 2024-10-20
Category: Announcement
Authors: Otto (Codeberg e. V.)

**tl;dr?** To make abuse prevention easier,
we are applying some default storage limits on Codeberg.
Free/libre software projects that need more will have to
[kindly request more resources](https://codeberg.org/Codeberg-e.V./requests).

Dear Codeberg users,
the title sounds like a contradiction,
but that's exactly how we mean it.

Ensuring a fair distribution of resources on a shared system is a difficult topic.
We repeatedly get suggestions to charge for certain services,
but we are strong believers in our donation-backed non-profit model.

I was thinking about the best way to introduce a quota feature for months.
I have seen the confusion and frustration on other platforms that changed their limits.
But there is one key difference with Codeberg:
We won't charge you for using more storage.
We want to provide you, the contributors to the free/libre software ecosystem,
with the resources you need for whatever you are creating.

Projects have different needs,
ranging from heavy CI/CD pipelines to storing large assets for your game,
or hosting large release assets.
We know that it's hard to find limits that work well for everyone.
Our goal is to find a line between "this is usually acceptable"
and "please [kindly ask us](https://codeberg.org/Codeberg-e.V./requests) first".


## Large projects on Codeberg

You might imagine that the largest projects on Codeberg are the most useful free software projects we host.
There are cool projects on Codeberg,
but many of them do not need a lot of storage.

What about the big repos?
Honestly, we don't know.
Next to a lot of obvious storage abuse like encrypted personal backups,
there are many projects that are not obviously useful to us,
but they might be useful to you.

Often, they lack an explanation about their use and their users.
Reviewing them manually,
making us familiar with the specific needs for certain project categories,
can be a tedious job.

If we ask users to request the resources they need,
this solves two problems in one go:
It gives maintainers peace of mind,
they don't have to worry their projects could be considered unreasonably large by our staff.
And it can distribute the load of reviewing project to more users,
something that the CI/CD request workflow successfully demonstrated to work.


## But storage is cheap!

It is not.
All data on Codeberg probably fits into a 12 TB drive.
But storage is much more complex than that:

Storage needs to be fast,
serving thousands of simultaneous file accesses from one cheap 12 TB drive is not possible.
We don't want you to wait,
so we invested into fast SSDs.

Storage needs to be secure:
While you might have backups,
we don't want you to need them.
Data is replicated, and backed up offsite,
which requires more disks,
and network bandwidth.

Storage needs management:
Building the architecture for Codeberg needs human resources.
I have spent months learning about Ceph,
our distributed storage,
We are currently expanding our cluster,
carefully planning each step.
We are running maintenance jobs such as backups, filesystem checks,
garbage collection.
Every stored byte complicates the situation.

Storage needs to be served:
From CI/CD pipelines to users downloading your software
or importing it as a dependency,
we are proud to benefit much more than developers that chose to publish on Codeberg.
However, search engines and AI scrapers crawling large content
significantly adds to the load on our systems, too.
We prefer when this content is content that we actually want to host.

Storing data can be cheap.
Making sure that the data is accessible when you need it is costly.


## Forgejo to the rescue

The recent [release of Forgejo v9](https://forgejo.org/2024-10-release-v9-0/) comes with a very important feature:
Storage quotas.
They put limits to the majority of abuse,
and allow granting exceptions for projects after a quick review.

Together with a Forgejo user in the education sector,
we have started to develop the required code to limit storage in a flexible way.

We are currently testing the feature and will enable it soon,
carefully tweaking it to the needs of our users.
Thanks to all Forgejo contributors who made this possible.


## What is changing?

Our offer does not change:
We will still grant every project the resources it needs,
provided that we can afford them.

Over the course of several months, we will slowly tune the quota rules.
Currently, **the following limits will apply** to Codeberg users and orgs by default:

- Git repository storage: 750 MiB
- LFS, Packages, Releases, Attachments: Additional 1.5 GiB
- Additionally, the total of "non-promoted" content,
that is private and personal repositories ("my website", "my dotfiles"),
should not exceed 100 MiB.

If you know that you require more storage than we grant by default,
kindly let us know via the [request procedures](https://codeberg.org/Codeberg-e.V./requests).
We are confident we'll figure something out!

If you exceed the limits and have not requested an exception,
you will not be able to upload to Codeberg.
There are currently no plans for removing existing content.

We will be careful in adjusting the limits,
aiming for zero friction for those who use our service.

Do not hesitate to contact us with any questions or ideas you might have.


## Some more background …

The implementation of the quota follows a decision of the annual assembly of Codeberg e.V. members from May 2023.
Initially, the new rules were scheduled to take effect in June 2024,
but technical constraints delayed this.

Now that we have the quota feature,
we are finally able to enforce the decision.

In the past week, Codeberg has faced significant slowness and some downtime,
most of which originates from illegitimate traffic such as new piracy content,
excessive crawling and other activity that does not come from members of the free/libre software movement.

Although we are currently planning on expanding our infrastructure,
making Codeberg more resilient against these kinds of issues,
we believe that a default storage quota will be a key in providing service to those who need it:
the free/libre software contributors.

It will take time for the measures to provide a positive effect,
but we are sure it will happen.

Thank you for your understanding.
We are looking forward to your contribution,
no matter if it needs less or more than the default limits,
and we will continue to ensure that we provide you with the service you need.
