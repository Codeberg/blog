Title: Monthly Report June 2020
Date: 2020-07-15
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V this month; as usual published here with due delay.)


Dear Codeberg e.V. Members and Supporters!

Once again it is time for some updates.

After a lot of paperwork Codeberg e.V. finally became a "gemeinnütziger Verein", which roughly tranlates into "charitable organization recognized by tax authorities". That means we will have to pay less taxes and are allowed to send out annual contribution receipts to members and donators.

A few weeks ago we also migrated codeberg to gitea 1.12. Due to the vast number of new features this needed a longer testing phase than usual. We want to thank the gitea maintainers for their great work on the 1.12 release! The new release also lets the user specify the default branch name when creating a new repository, the UI default setting is now 'main'.

We are hosting 4054 repositories, created and maintained by 3171 users. Compared to one month ago, this is an organic growth rate of +604 repositories (+17.5% month-over-month) and +353 users (+12.5%)

Codeberg e.V. has 70 members in total, these are 53 members with active voting rights and 17 supporting members.

Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username.

Your Codeberg e.V.

--<br/>

https://codeberg.org
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany
Registered at registration court Amtsgericht Charlottenburg VR36929. 

