Title: Monthly Report March 2019
Date: 2019-04-12
Category: Letters from Codeberg
Authors: Codeberg e.V.

(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V one week ago)

Dear Codeberg e.V. Members and Supporters!

It is time for some updates.

Codeberg e.V. has 29 active members. We are hosting 477 repositories, created and maintained by 461 users. Compared to one month ago, this is an organic growth of rate of +64 repositories (+15% month-over-month), and +40 users (+10%).

The machines are runnnig at less than 25% capacity, we will scale up as soon we come close to the 66% threshold. Backups are still managed by private offline machines by founding members at no cost; as soon Codeberg e.V. can afford it's own infrastructure, we are planning for an economic mid-end server with extendable redundant RAID disks, hardware donations may or may not allow us to afford those earlier. For the live server, our models suggest that rented cloud VMs are the most economic option for up to ~1000 users/repos, from then on it would potentially make sense to switch to rented dedicated servers.

Where are we going next? As we wrote in our previous update, we need to incorporate a minor bylaw change to get recognized as Non-Profit organization by tax authorities. We will propose this change as pull request, and will ask our members for vote via email.

Since the latest release, Gitea 1.8, the user-facing frontend we use for Codeberg.org, has incorporated support to act as Oauth2 provider. We are currently testing this protocol, this will hopefully facilitate incorporation of additional features (for a list of ideas, please see https://codeberg.org/Codeberg/Community/issues/23 – don't hesitate to contact us if you like to contribute). Also, Gitea is now coming with Dark Theme. Visit your user account settings and select 'arc-green'.

We are waiting for feedback from the Gitea developer team for our pull request that introduces public editable wikis (enabled and controlled in the repository settings).

Long-term, it would be great to have informative English translations of all relevant documents (some official documents are still German-only as this is the language required by local authorities). If you would like to contribute, translations, PRs in the org repository are very welcome!

We look forward to attract more users to build and strengthen the social network of developers meeting on Codeberg.org. Let's spread the word! We are looking forward to exciting times.

Codeberg e.V.

