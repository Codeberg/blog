Title: Monthly Report March and April 2022
Date: 2022-05-10
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the monthly news letters sent out to members of Codeberg e.V.; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters!

Let's share some news from March and April.


**Landing page renovation**: We now have a mockup for a new landing page thanks to user mray. We are now trying to turn this into reality. If you have some decent knowledge about frontend development and responsive design, consider helping out. Just send a mail to otto@codeberg.org.

**Gitea Update**: We are running 1.16 since early March and looking forward to the next release. You can find details about the deployment in a [dedicated blog article](/introducing-gitea-116.html).

**Datacenter Outage**: On March 10, we had a quick downtime of our dedicated server in a Berlin datacenter. This was due to a routing issue at IN Berlin which was resolved after about 15 minutes. Still this shows us, that we need to go for redundancy among DCs in the future.



## And the usual number games:

In march we were 23391 repositories, created and maintained by 20049 users. Compared to one month ago, this is an organic growth rate of +1936 repositories (+9% month-over-month) and +1136 users (+6%).
In April, we were hosting 25120 repositories, created and maintained by 21362 users. Compared to one month ago, this is an organic growth rate of +1968 repositories (+8.5% month-over-month) and +1345 users (+6.7%).

The storage requirements on our machines went up to about 60%, and we started looking for distributed file systems.


Codeberg e.V. members in March / April were 172/183 members in total, these are 123/131 members with active voting rights and 48/51 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.

