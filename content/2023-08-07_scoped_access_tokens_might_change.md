Title: The permissions for your scoped access tokens might change on Thursday
Date: 2023-08-07
Category: Announcement
Authors: Codeberg Team


Dear Codeberg users,

we are excited to deploy Forgejo v1.20 to production, bringing tons of new features, bugfixes and improvements.

However, there's a caveat with scoped access tokens: They will change, eventually granting more permissions than expected. Please read on if you use them. We additionally email all users who are known to be affected.

On **THURSDAY, AUGUST 10 2023**, the new Forgejo version will be deployed and your **EXISTING SCOPED TOKENS** might receive **MORE PERMISSIONS THAN YOU INITIALLY SELECTED.**

The migration will try to match the old scopes as close as possible, but some granularity will be unavailable. If you rely on it for security reasons, please revoke your access tokens before Thursday.

**The new implementation** will only allow to grant "no", "read" or "write" permissions to the following sections individually:

- Repository
- Organisation
- User
- ActivityPub
- Package
- Issue
- Notification

If you want to **continue** using your tokens with **resulting expanded scope**, or if you created tokens with **all permissions**, **no action** from your side is required.

Codeberg.org runs on Forgejo, a soft-fork from the Gitea project. While we both do add our own patches and features, deviating from Gitea for such an essential part of the codebase is currently not feasible due to limited human power. Therefore, we have decided to stick with the behaviour implemented by Gitea, although we find the situation very unfortunate and would have loved for a longer transitioning period. We ask for your understanding.

If you have further questions or require assistance, please let us know.

If you are nerdy enough to dive into details about this, you can find further information about the new scoped access tokens in the [original Pull Request](https://github.com/go-gitea/gitea/issues/24501) and by [reading the code where old scopes are going to be matched with the new scopes](https://codeberg.org/forgejo/forgejo/src/commit/d52f70447f83f848c3d040670e6afb4780f618de/models/migrations/v1_20/v259.go#L283-L315).

Kind Regards  
Your Codeberg Team

