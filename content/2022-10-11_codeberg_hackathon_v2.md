Title: Codeberg Hackathon goes v2
Date: 2022-10-22
Category: Events
Authors: Otto (Codeberg e. V.)


*TL;DR: We invite you to take part in the Codeberg Hackathon on the weekend starting Friday, 04th November 2022.*

We're doing it again:
A hackathon for Codeberg users, e.V.-members, interested developers, Free Software lovers and everyone else
who wants to lend us a helping hand.

The goal sounds simple, but requires our combined forces:
We aim for making Codeberg and its ecosysstem around Gitea, Woodpecker CI and the systems
an awesome and truly libre alternative to proprietary platforms for Software and Content development.
We want to bring the development community of Codeberg together,
and allow everyone to help in building their project home together.

We already had such a hackathon from 8th to 10th of April 2022,
and we'll repeat it on one of the coming weekends (post will be updated with poll results).

**Relevant Resources:**  
[HedgeDoc Pad for project planning etc](https://pad.ccc-p.org/kIRY5hpsRZemFQlMQm_60Q#)  
[Matrix Channel “Contributing to Codeberg”](https://matrix.to/#/!wIJGSzCYITbuEkORkq:matrix.tu-berlin.de?via=wuks.space&via=matrix.org)  
[Meeting Room (BigBlueButton)](https://lecture.senfcall.de/ott-qfr-xes-p20)


## The Roadmap

We'll start a get-to-know session on Friday evening (European time),
and discuss projects and ideas already.

The actual coding on our main projects starts Saturday and continues until Sunday evening.

We'll be hacking on one or more projects together,
always allowing individuals to take over simple and independent tasks if they prefer to work on their own.

Projects we might be hackin on together:

- Sync docs with Gitea upstream [comment](https://github.com/go-gitea/gitea/issues/15736#issuecomment-1245229377)
- implement OAuth scopes for Gitea
- implemt content reporting and simple admin API for Gitea

You can additionally always work on one of the following topics on your own:

- check Codeberg Community Issues and forward to upstream if not already done
- some a11y and frontend issues (we'll provide a list)

## The schedule

This event is community-driven and powered by people like you,
thus this schedule is only an estimate: Times might differ depending on personal circumstances.
Yet, this is what we are currently aiming for (and we'll update it here in the blog post if necessary).
**If not stated otherwise, all times are UTC**.

**Friday, November 4th 2022:**

- 19.00 UTC (20.00 CET): Opening, presentation of ideas and brainstorming
- ~ 19.30 UTC (20.30 CET): Get-together and open hacking: Chill the evening and talk to like-minded people, or already get started with your individual project (open end)

**Saturday, November 5th 2022:**

- From 09.00 UTC (10.00 CET): Meeting in project group(s) and working on the project, Codeberg admins and maintainers will try to be around to answer your questions.
- 19.00 UTC (20.00 CET): Evaluation of day one, agendasetting for Sunday
- 19.30 UTC (20.30 CET): Get-together and chill (or further hacking if you prefer, open end)

**Sunday, November 6th 2022:**

- From 09.00 UTC (10.00 CET): Meeting in project group(s) and working on the project,
- 19.00 UTC (20.00 CET): Presentation of results, evaluation of the event, discussing further plans
- 19.30 UTC (20.30 CET): Get-together and chill (or further hacking if you prefer, open end)


Thank you very much!


## Questions and answers

### Do I need to register to participate?

You don't need to. But if you tell us that you're coming (ping in the Matrix channel or email to otto@codeberg.org),
we can count you in. Please tell us what you're interested in working on.

### Are there requirements for helping out? Do I have to know (...how to code / ...a programming language / ...about some codebase)?

No, we welcome everyone, even newcomers and people that want to learn something.
While we can't guarantee that someone is around to teach you something,
we'll do our best to make sure you learn something if you are interested.

In any case, we'll find something to do for everyone - so if you can spare the time and want to help out, just join us!

### I can't attend the full weekend, is this a problem?

No, please join anyway. You do not have to attend the full time.
We are doing two presentations of projects, to grant more people the chance to get in.
We are also doing some scheduled onboarding, if you want to join later.
As long as someone from the team is online, you can join any time and leave any time,
but if you took the responsibility for a project, we'd be happy to stay in touch with you about the future.

### What if I can't finish my idea on the weekend?

That's not a problem, in fact to be expected for everything that goes beyond a tiny fix.
If you are working in a work group, we'll try to discuss the future of the project on Sunday.
If you have a tiny project on your own, please get and stay in touch with us and we'll make a plan for how to move on.
In any case, we appreciate your contribution, and we appreciate even more if you commit to finishing your started work afterwards.

### I just joined the meeting, but no one is online.

We can't guarantee to always have people around, but we'll surely be there during the announced onboarding times.
If you can, please wait a moment to see if someone joins you, or write a short message in the Matrix group.

### Your times absolutely don't work for me

If you live in a part of the world (or have a completely different day/night rhythm) that doesn't align well with our schedule,
we are very sorry. Please give us feedback in that case.
We are looking forward to doing more events of that kind in the future,
and we'd love to adapt to the wishes of our users and members.
