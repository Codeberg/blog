Title: Codeberg is moving ... and what this means to you
Date: 2022-11-13
Category: Announcement
Authors: Otto (Codeberg e. V.)


Codeberg is moving, and you can imagine,
moving a mountain takes time and effort,
and might have a rocky road.

**TL;DR: Codeberg might suffer from decreased performance,
and smaller downtimes from Monday (Nov 14) to Wednesday (Nov 16).
Final migration will require a short period of planned downtime on Wednesday.**

The great news first: Codeberg is growing,
more and more users decide to get a free/libre home for their projects.
And they are trusting the non-profit Codeberg e.V. to take care for them.
This is great. And we are doing our best to give everyone a home you'd love to live in.
But our first mountain (German: Berg) is getting tight and cuddly to fit everyone in.

We have been looking for this moment for more than a year,
since we installed our own hardware in Berlin.
We are running Woodpecker CI, Codeberg Pages, Weblate Translate and more on it.
But the heart of Codeberg – Gitea – is still on a rented cloud instance.

It is past time to leave our rented infrastructure for a bright future.
Our own server has plenty of spare resources,
but our cloud instance is suffering from high load,
and storage is running low.
So finally we can distribute the resources better and make everyone happy again.

Now is the moment to do the move.
The team is excited, we have some helping hands,
and our host named ["Kampenwand"](https://en.wikipedia.org/wiki/Kampenwand) is welcoming you
and your projects.
We're coming!


## What does this mean to you?

We're trying to keep the impact on our users as small as possible.
But our current setup makes a zero-donwtime migration very, very hard.

Our human resources aren't unlimited,
and we want to stay efficient.
We are accepting short periods of unavailability
to make this project much easier and save the energy for more projects.

We have a remote Ceph filesystem mounted on the old cloud VPS,
and already moved many repositories to it
(especially archived content and large private repositories).
But we noticed that the heavy delay between the servers sometimes causes problems.
As we move more data, we expect occassionally decreased performance,
and there might be short periods of downtime,
but we promise to monitor closely.

If we have everything prepared by Wednesday,
we are going to do the final migration between 10.00 UTC and 22.00 UTC.
We'll have planned periods of downtime during that day, but aiming to keep them minimal.


## How can you help?

Short-term, coordinating support is very hard,
since we're mostly busy during the next days.
If you have free time you'd like to provide,
feel free to give us a ping in the [Contributing to Codeberg Matrix Channel](https://matrix.to/#/!wIJGSzCYITbuEkORkq:matrix.tu-berlin.de?via=wuks.space&via=matrix.org).

Please don't ping us on social media about general downtimes.
Our monitoring is quicker anyway,
and filtering through the noise is difficult.

If, however, something on the platform breaks (e.g. the access to your repos),
and isn't resolved after 30 minutes, feel free to open an issue in our Community Issue Tracker.

Last but not least, you make our days shorter if you
**don't rename your user, projects, organizations or transfer repositories**.
Just as a precaution.
Thank you :)

Other than that, wish us best luck,
and consider a donation for the next hardware upgrade.

Kind Regards & thank you for your patience!  
Your Codeberg Sysadmins :)

