Title: Codeberg Docuthon in July 2021
Date: 2021-07-07
Category: Events
Authors: Codeberg e.V.


*TL;DR: We invite you to take part in the Codeberg Docuthon from July 10 to July 17 and write some documentation about your favourite project - or collaborate on the Codeberg Docs.*

We at Codeberg are very proud to be the home to many awesome software projects. As of July 2021, there are about 15k repositories and Codeberg users have created more than a million actions (comments, pushes etc), and we are convinced that many of you are living FLOSS by heart, improving the world day to day.

But what makes good code usable in the end? Is it enough to push your work to some repo and give it a licence? Sadly, the topic isn't that easy, although it's already a very good start to make sure your project has proper licensing so others can make use of it. Another important key to reusing software is telling others about how to make use of it. This is where documentation comes into the game.

## Document your code!

From time to time it's good to take a step back and look at your work through fresh eyes. What did you create? What does it do? What can others use it for? And tell them!

On Codeberg, we are sometimes coming about repos that don't even have a README, or it is only telling you that this is some library for thing-foo-bar. Sure, you shared the code already, so others can have a look at it, (try to) understand it, integrate and use it. But seriously, how likely are you to dive into something completely unknown? Wouldn't it be better if you had an interesting README, some docs and some comments that raise your interest?

We think that **documentation is very important to software usability**, so we invite you to take the time and write it.

## Join the Docuthon!

You think so, too? Take a step back and join the Codeberg Docuthon from July 10 to July 17.

If you have your own software projects to work on:

- think of where your Code Documentation can be improved
- add the missing pieces or indicate the missing parts to others
- add the "Docuthon" flag to your repo, so others can find it (and help if needed)

If you want to contribute somewhere else or need a break:

- [check out other Docuthon repos and their progress](https://codeberg.org/explore/repos?q=Docuthon&topic=1), give feedback and see if they are useful to you
- ask if you can help explain the features in an understandable manner
- we also want to take the opportunity to give the Codeberg Docs a sprint, check out our [open issues](https://codeberg.org/Codeberg/Documentation/issues) - the Documentation maintainers are around to help you and answer questions. You can also join the [#codeberg-documentation:matrix.org](https://matrix.to/#/#codeberg-documentation:matrix.org) Matrix channel.

## Nice side effects

Documenting your code does not only benefit users. It might save you some unnecessary support requests, it can even allow you to take a breath, look at what you have done so far and think about your software design. In the spirit of [Readme Driven Development](https://tom.preston-werner.com/2010/08/23/readme-driven-development.html), you can spend some time on thinking about your software design before wildly changing the code, and it also lowers the barrier for other developers to join your project if they get to know what capabilities already exist and what you are going for next, instead of reading through all the code in order to finally understand the project before doing any changes.
