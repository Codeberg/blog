Title: Monthly Report November 2021
Date: 2021-12-15
Category: Letters from Codeberg
Authors: Otto (Codeberg e. V.)

*(This is a stripped-down version of the monthly news letter sent out to members of Codeberg e.V. this month; as usual published here with due delay. Not member of the non-profit association yet? [Consider joining it!](https://join.codeberg.org))*

Dear Codeberg e.V. Members and Supporters, once again it is time for some updates!


We are hosting 18791 repositories, created and maintained by 16585 users. Compared to one month ago, this is an organic growth rate of +1302 repositories (+7.4% month-over-month) and +1052 users (+6.8%).

The year is coming to an end, and we wish everyone some calm next weeks. Remember: If your membership fees are paid anually, it's time for collection in January. If you want to change your amount of contribution, now is a good time to do so.

**License reminders:** Early in November, we started popping up license reminders for repos that do not yet carry a valid FLOSS license as defined by either the FSF or the OSI. They are working great so far and it feels good to see many projects now finally be freed in a legal sense, too. We'll continue to keep an eye on resource abuse in the next months to make sure your financial contributions do support the Free Software movement.


The machines are running at about 46% capacity (partly due to garbage collection and cleanup of quarantined repositories), as usual we will scale up once again we approach the 66% threshold.

Codeberg e.V. has 163 members in total, these are 118 members with active voting rights and 44 supporting members, and one honorary member.


*Friendly reminder: membership in the account group "Members" on codeberg.org is not automatic (this group enables access to Codeberg e.V.'s internal repos and discussion between Codeberg e.V. members therein). For privacy reasons we add members on request (your membership is visible to other members). If you are not yet in this account group, but would like to join, please send an email to codeberg@codeberg.org and tell us your username. If you need access to Codeberg e.V. report documents without being listed in the account group, please send an email.*

Your Codeberg e.V.

--<br/>

https://codeberg.org<br/>
Codeberg e.V. – Gormannstraße 14 – 10119 Berlin – Germany<br/>
Registered at registration court Amtsgericht Charlottenburg VR36929.
