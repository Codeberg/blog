Title: We stay strong against hate and hatred
Date: 2025-02-12
Category: Announcement
Authors: Codeberg public relations team

Codeberg is currently suffering from hate campaigns due to far-right forces, and so are our users. First and foremost, we apologize for everyone who has recently received a notification email from our system containing offending and potentially traumatizing content. We are working hard on containing the effects on our users and systems.

Most importantly, **your private data was not leaked**. All emails have been generated through Codeberg's servers using the notification feature and the abusers had **no access to your email address** directly.

Fighting hate and far-right forces is important to us. Read to the end of this article to learn why.

## What has happened?

In the past days, several projects advocating tolerance and equal rights on Codeberg have been subject to hate attacks, such as massive spam of abusive messages in their issue trackers. We have been monitoring the situation closely and have tried to clean up the content as quickly as possible.

Often, content remained available only for a few and up to 30 minutes. Due to constrained personal capacities, some rare cases have remained online for longer. We appreciate all your reports to abuse@codeberg.org that help us identify abuse quickly.

On 12 February 2025, an abuser has escalated the attacks to a next level. Instead of targetting individual projects, they have started to create abusive content and mentioned Codeberg users in chunks of 100 each. Depending on the notification settings of users (if you are a user on Codeberg, you can modify it [in your settings](https://codeberg.org/user/settings/account)), these generate notification emails that contain a copy of the post that includes the mention - and thus the abusive content via email.

Although our staff reacted quickly, blocked access to the used functionality and deleted the user accounts, they managed to generate a large amount of notification emails already.

Since this attack is not only harming Codeberg users but the platform itself, both via technical means (we had brief downtime of our systems and our mail server was suffering a lot), as well as by harming the reputation of our platform and trust users have shown us. We expect this incident to be in response to our swift moderation of the previous campaigns that targeted only individual projects.

## How could this happen?

Please understand that Codeberg.org is driven by volunteers mostly. We have sufficient capacity to run the platform under normal conditions, and normal conditions also include some headway to deal with abuse campaigns that come and go. We suppose that most readers did not notice the spam campaigns of the past days, and we are happy that we managed to contain them quickly. We are fighting with advertisement spam, phishing and malware week to week.

However, our capacity was obviously exhausted this time, when it comes to attacks that target us specifically. And we apologize for this.

You might wonder why there are not more technical countermeasures in place to prevent this type of abuse. Technical measures evolve over time, and we have implemented several protections on multiple levels that are trying to contain the amount of abuse you see day to day.

However, doing proper rate-limiting is hard. We need to ensure that legitimate usage of our API and interface is possible, including custom scripts that import or synchronize massive amounts of issues from projects on other platforms. There are some rate-limits tailored to the previous abuse vectors we have seen, which was mostly aggressive advertizing, and so it would not have been possible to create these massive postings if they would have contained a hyperlink to another website, for example.

However, just spamming notification emails to users is a new abuse vector to us, and we did not sufficiently prepare for this. For that, we are sorry.

## What will Codeberg do?

Currently, we are investigating the details of the attack and we have implemented short-term countermeasures and monitor activity on the platform closely. Further, we are responding to hundreds of emails from our users that ask about the incident. Some request the deletion of their data in response.

Next up, we will make plans on how to improve our protection against this and future kinds of abuse attacks on Codeberg itself to reduce the likelihood of similar things from ever happening again.

If you want to help with development work on [Forgejo](https://codeberg.org/forgejo/forgejo/), the free/libre and open source software that powers Codeberg, please reach out. Go developers who can commit some time and patience to implement one our more technical measures in the codebase are much appreciated.

Unrelated to the current incident, we have worked with [NLnet](https://nlnet.nl/) in the past weeks and secured funding for some moderation tools. A meeting was scheduled for later today at 20.00 CET to talk about technical architecture for a feature that allows reporting abuse directly in the app and the goal is to implement it for the next release of Forgejo.

## Far-right forces endanger free/libre software projects

We will not be discouraged in our fight against far-right ideologies. They are currently on the rise in many parts of the world, and we believe it is important to protect all kinds of marginalized groups. However, if you believe this does not affect your project, you are wrong. Far-right forces pose a threat to all of us.

Extreme right forces actively target members of our communities and discriminate based on ethnicity and gender, political background, sexual orientation, disabilities, nationality and faith. However **diversity is an important asset** in free/libre software communities and it is what makes our software great and development productive.

By targetting some of our most active translators, nicest designers, best developers and all other motivated contributors, they are hurting the free/libre software ecosystem as a whole.

Don't be fooled if right-wing forces promise to "promote open source" in their political agenda. This has nothing to do with the values of our movement! This is about national patriotism and protectionism, and they will happily accept splitting our community on their way.

We all know that the free/libre software ecosystem won't work this way. Every human is an integral and equally important part of it, and targetting some of our community members threatens the ecosystem as a whole.

Let's together stay strong and united against the emerging threats. We stay strong against discrimination of all kind, including but not limited to sexism, transphobia, homophobia, racism, antisemitism and ableism. And we hope that you all join us for this mission.

A big shout-out to all the projects that collect facts and resources against hate and discrimination and that have been the primary goal of these attacks. Support them if you can.

If you ever considered supporting the fight against right-wing forces, for example by joining political movements and parties, organizing protests or getting involved in online communities with this goal - now might be the best time to move ahead.

Again, we apologize for the disruption of your work and the abusive content delivered to your mailboxes. We are doing our best to contain the situation.

We'd like to thank everyone who signaled their support in the current situation, via e-mail, Mastodon and in Matrix chats. This means a lot to us.

Thank you for your trust and support!  
Your Codeberg Public Relations team
