#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals
from datetime import date

AUTHOR = u'Codeberg e.V.'
DESCRIPTION = u'Codeberg is a non-profit, community-led organization that aims to help free and open source projects prosper by giving them a safe and friendly home.'
SITENAME = u'Codeberg News'
SITEURL = 'https://blog.codeberg.org.org'
SITEIMAGE = 'https://design.codeberg.org/logo-kit/icon.svg'

PATH = 'content'
OUTPUT_PATH = 'tmp/blog'

TIMEZONE = 'Europe/Paris'
STARTYEAR = '2019'
# comment CURRENTYEAR out if it's the same as STARTYEAR
CURRENTYEAR = date.today().year

DEFAULT_LANG = u'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None

DELETE_OUTPUT_DIRECTORY = True

THEME = 'themes/codeberg'

# Blogroll
LINKS = (('Codeberg', 'https://codeberg.org/'),
         ('Join Codeberg e.V.', 'https://join.codeberg.org'),
         ('RSS Feed', 'https://blog.codeberg.org/feeds/all.atom.xml'),)

# Social widget
SOCIAL = (('Mastodon', 'https://social.anoxinon.de/@Codeberg'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True
